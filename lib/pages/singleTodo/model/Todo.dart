class Todo {
  static int idCounter = 0;
  int id;
  String title;
  String description = "important task that i should do!";
  bool isDone = false;
  DateTime deadLine;
  int reminderDaysCount;
  Category category;

  Todo(
      {required this.id,
      required this.title,
      required this.description,
      required this.isDone,
      required this.deadLine,
      required this.reminderDaysCount,
      required this.category});
}

class Category {
  static Category NO_CATEGORY = Category(name: "NO_CATEGORY");
  static int idCounter = 0;
  String name;

  Category({required this.name});
}

class FormErrors {
  bool title = false;
  bool alertDays = false;
  bool deadline = false;
}
