import 'dart:ffi';
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:todoflutterapp/pages/singleTodo/model/Todo.dart';
import 'package:bs_flutter_selectbox/bs_flutter_selectbox.dart';

class SingleTodoPage extends StatefulWidget {
  final Todo? todo;
  final List<Category> categories;
  final List<Todo> todoList;
  final Function addOrEditTodo;

  const SingleTodoPage(
      {super.key,
      this.todo,
      required this.categories,
      required this.todoList,
      required this.addOrEditTodo});

  @override
  State<SingleTodoPage> createState() => _SingleTodoPageState();
}

class _SingleTodoPageState extends State<SingleTodoPage> {
  String action = "New";

  late TextEditingController titleTextFieldController;
  late TextEditingController descriptionTextFieldController;
  late TextEditingController reminderDaysCountTextFieldController;
  late TextEditingController datetimeTextFieldController;
  late BsSelectBoxController _selectBoxController;
  late TextEditingController addCategoryTextFieldController;

  late String titleField;
  late String descriptionField;
  late String alertDaysField;
  late String deadlineField;
  late Category selectedCategoryField;

  late FormErrors formErrors;
  late bool formSubmittedForTheFirstTime = false;

  @override
  void initState() {
    super.initState();

    //debug code
    print(widget.categories.length);
    //debug code

    formErrors = FormErrors();
    titleTextFieldController = TextEditingController();
    descriptionTextFieldController = TextEditingController();
    reminderDaysCountTextFieldController = TextEditingController();
    datetimeTextFieldController = TextEditingController();
    _selectBoxController = BsSelectBoxController(
        options: widget.categories
            .map((e) => BsSelectBoxOption(value: e.name, text: Text(e.name)))
            .toList());
    addCategoryTextFieldController = TextEditingController();
    action = widget.todo != null ? "Edit" : "New";

    titleField = widget.todo != null ? widget.todo!.title : "";
    descriptionField = widget.todo != null ? widget.todo!.description : "";
    alertDaysField =
        widget.todo != null ? widget.todo!.reminderDaysCount.toString() : "";
    deadlineField = widget.todo != null
        ? DateFormat('yyyy-dd-MM').format(widget.todo!.deadLine)
        : "";
    Category category =
        widget.todo != null ? widget.todo!.category : Category.NO_CATEGORY;
    selectedCategoryField = category;

    _selectBoxController.setSelected(
        BsSelectBoxOption(value: category.name, text: Text(category.name)));
  }

  bool validate() {
    bool validated = true;

    if (titleField == "") {
      validated = false;
      setState(() {
        formErrors.title = true;
      });
    }
    if (alertDaysField == "") {
      validated = false;
      setState(() {
        formErrors.alertDays = true;
      });
    }
    if (deadlineField == "") {
      validated = false;
      setState(() {
        formErrors.deadline = true;
      });
    }

    return validated;
  }

  void onSubmitButtonTap() {
    if (!formSubmittedForTheFirstTime) formSubmittedForTheFirstTime = true;
    if (validate()) {
      Todo newTodo = Todo(
          id: widget.todo != null ? widget.todo!.id : Todo.idCounter,
          title: titleField,
          description: descriptionField,
          isDone: widget.todo != null ? widget.todo!.isDone : false,
          deadLine: DateFormat('yyyy/dd/MM').parse(deadlineField),
          reminderDaysCount: int.parse(alertDaysField),
          category: selectedCategoryField);
      widget.addOrEditTodo(newTodo, action);
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text("Done :)"),
        backgroundColor: Colors.green,
        duration: Duration(milliseconds: 1000),
      ));
      Future.delayed(Duration(milliseconds: 1000), () {
        Navigator.pop(context);
      });
    } else {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text("Please Fill The Required Fields!"),
        backgroundColor: Theme.of(context).errorColor,
        duration: Duration(milliseconds: 1000),
      ));
    }
  }

  void onAddCategoryTap() {
    bool isFound = false;
    if (addCategoryTextFieldController.text == "") return;
    for (var i = 0; i < widget.categories.length; i++) {
      if (addCategoryTextFieldController.text == widget.categories[i].name) {
        isFound = true;
        setState(() {
          _selectBoxController.setSelected(_selectBoxController.options[i]);
          addCategoryTextFieldController.clear();
        });
        break;
      }
    }
    if (!isFound) {
      setState(() {
        Category newCategory =
            Category(name: addCategoryTextFieldController.text);
        widget.categories.add(newCategory);
        print("categories len : ${widget.categories.length}");
        _selectBoxController = BsSelectBoxController(
            options: widget.categories
                .map(
                    (e) => BsSelectBoxOption(value: e.name, text: Text(e.name)))
                .toList());
        _selectBoxController.setSelected(BsSelectBoxOption(
            value: newCategory.name, text: Text(newCategory.name)));
        addCategoryTextFieldController.clear();
      });
    }
  }

  void onDateTimeFieldTap(BuildContext context) async {
    final DateTime? selectedDateTime = await showDatePicker(
        context: context,
        initialDate: DateTime.now(),
        firstDate: DateTime.now(),
        lastDate: DateTime(2025));
    if (selectedDateTime != null) {
      setState(() {
        datetimeTextFieldController.text =
            "${selectedDateTime.year}/${selectedDateTime.day}/${selectedDateTime.month}";
      });
    }
  }

  Widget categoryFieldSelection(BuildContext context) {
    return Container(
      height: 50,
      width: MediaQuery.of(context).size.width,
      child: BsSelectBox(
          onChange: (value) {
            selectedCategoryField = Category(name: value.getValueAsString());
          },
          style: BsSelectBoxStyle(
              border:
                  Border.all(width: 1, color: Theme.of(context).disabledColor),
              backgroundColor: Theme.of(context).scaffoldBackgroundColor),
          size: BsSelectBoxSize(),
          hintText: "Select a Category",
          controller: _selectBoxController),
    );
  }

  Widget categoryCreatorField(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      width: MediaQuery.of(context).size.width,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        // mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Container(
            width: MediaQuery.of(context).size.width - 83,
            child: TextField(
              controller: addCategoryTextFieldController,
              textCapitalization: TextCapitalization.words,
              decoration: InputDecoration(
                  border: OutlineInputBorder(borderSide: BorderSide(width: 3)),
                  label: Text("Or Create One"),
                  prefixIcon: Icon(Icons.category_sharp),
                  suffixIcon: InkWell(
                    onTap: () {
                      onAddCategoryTap();
                    },
                    child: Icon(Icons.add),
                  )),
              onChanged: (value) {
                setState(() {});
              },
              onSubmitted: (value) {
                onAddCategoryTap();
              },
            ),
          ),
        ],
      ),
    );
  }

  Widget categorySelectionColumn(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(10),
      decoration: BoxDecoration(
          border: Border.all(width: 1, color: Theme.of(context).disabledColor),
          borderRadius: BorderRadius.circular(5)),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          categoryFieldSelection(context),
          SizedBox(
            height: 10,
          ),
          categoryCreatorField(context)
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("${action} Todo")),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.deepPurple,
        onPressed: onSubmitButtonTap,
        child: Icon(action == "New" ? Icons.add : Icons.edit),
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 30.0, vertical: 16),
        child: Container(
          height: MediaQuery.of(context).size.height,
          child: ListView(
            children: [
              Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 20),
                    child: TextField(
                      onChanged: (value) {
                        titleField = value;
                      },
                      decoration: InputDecoration(
                        border: OutlineInputBorder(
                            borderSide: BorderSide(
                          width: 3,
                        )),
                        label: Text("Title"),
                        prefixIcon: Icon(Icons.title_sharp),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 20),
                    child: TextField(
                      onChanged: (value) {
                        descriptionField = value;
                      },
                      decoration: InputDecoration(
                        border: OutlineInputBorder(
                            borderSide: BorderSide(width: 3)),
                        label: Text("Description"),
                        prefixIcon: Icon(Icons.description_sharp),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 20),
                    child: TextField(
                      onChanged: (value) {
                        alertDaysField = value;
                      },
                      decoration: InputDecoration(
                        border: OutlineInputBorder(
                            borderSide: BorderSide(width: 3)),
                        label: Text("Alert me X days Before"),
                        prefixIcon: Icon(Icons.warning_amber_sharp),
                      ),
                      keyboardType: TextInputType.number,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 20),
                    child: TextField(
                      onChanged: (value) {
                        deadlineField = value;
                      },
                      controller: datetimeTextFieldController,
                      decoration: InputDecoration(
                        border: OutlineInputBorder(
                            borderSide: BorderSide(width: 3)),
                        label: Text("Dead Line"),
                        prefixIcon: Icon(Icons.timer),
                      ),
                      onTap: () {
                        onDateTimeFieldTap(context);
                      },
                    ),
                  ),
                  Padding(
                      padding: EdgeInsets.symmetric(vertical: 20),
                      child: categorySelectionColumn(context)),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
