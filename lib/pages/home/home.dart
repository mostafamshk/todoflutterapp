import 'package:flutter/material.dart';
import 'package:todoflutterapp/pages/singleTodo/singleTodo.dart';

import '../singleTodo/model/Todo.dart';

class Home extends StatefulWidget {
  const Home({super.key});

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  List<Todo> todoList = [];
  late List<Category> categories = [];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    categories.add(Category.NO_CATEGORY);
  }

  //Functions
  void addOrEditTodo(Todo todo, String action) {
    setState(() {
      if (action == "New") {
        setState(() {
          todoList.add(todo);
          Todo.idCounter++;
        });
      } else {
        setState(() {
          todoList.removeAt(todo.id);
          todoList.insert(todo.id, todo);
        });
      }
    });
  }

  //Widget functions
  Icon todoStatusIconPicker(Todo todo) {
    return todo.isDone
        ? Icon(
            Icons.done_all,
            color: Colors.green,
          )
        : todo.deadLine.isAfter(DateTime.now())
            ? Icon(
                Icons.warning,
                color: Colors.red,
              )
            : todo.deadLine.isAfter(DateTime.now().subtract(Duration(days: 2)))
                ? Icon(
                    Icons.warning,
                    color: Colors.orange,
                  )
                : Icon(
                    Icons.warning,
                    color: Colors.yellow,
                  );
  }

  Widget TodoSeperator(BuildContext context, String text) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            text,
            style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
          ),
          Container(
            width: MediaQuery.of(context).size.width,
            height: 2,
            color: Colors.white,
          )
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Your Todo List"),
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.deepPurple,
        child: Icon(Icons.add),
        onPressed: () {
          Navigator.push(context, MaterialPageRoute(
            builder: (context) {
              //debug code
              print(categories.length);
              //debug code
              return SingleTodoPage(
                categories: categories,
                todoList: todoList,
                addOrEditTodo: addOrEditTodo,
              );
            },
          ));
        },
      ),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: todoList.isEmpty
            ? Center(
                child: Text(
                "No ToDo Added Yet!",
                style: TextStyle(
                    fontSize: 25,
                    fontWeight: FontWeight.w600,
                    color: Theme.of(context).disabledColor),
              ))
            : ListView.builder(
                itemBuilder: (context, index) {
                  Todo todo = todoList.elementAt(index);
                  return ListTile(
                    onTap: () {
                      Navigator.push(context, MaterialPageRoute(
                        builder: (context) {
                          return SingleTodoPage(
                            todo: todo,
                            categories: categories,
                            todoList: todoList,
                            addOrEditTodo: addOrEditTodo,
                          );
                        },
                      ));
                    },
                    onLongPress: () {},
                    leading: Container(
                      width: 30,
                      height: 30,
                      decoration: BoxDecoration(shape: BoxShape.circle),
                      child: todoStatusIconPicker(todo),
                    ),
                    title: Text(todo.title),
                    subtitle: Text(todo.description),
                    trailing: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          // Text("hello"),
                          Checkbox(
                            value: todo.isDone,
                            onChanged: (value) {
                              setState(() {
                                todo.isDone = !todo.isDone;
                              });
                            },
                          )
                        ]),
                  );
                },
                itemCount: todoList.length,
                scrollDirection: Axis.vertical),
      ),
    );
  }
}
